FROM ruby:2.3.3

RUN mkdir /challenge
WORKDIR /challenge

ADD Gemfile .
ADD Gemfile.lock .

RUN bundle install

ADD . /challenge